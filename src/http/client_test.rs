use crate::http::{get_point_data, get_url, MockHttpBase};
use crate::utils::{load_config, Config};
use crate::PointDatum;
use mockall::predicate::eq;

#[test]
fn test_get_url() {
    let config = load_config().unwrap();
    let config = Config {
        api_url: "foo".to_string(),
        device_0_id: "qux".to_string(),
        ..config
    };
    let url = get_url(&config);
    assert_eq!(url, "foo/devices-0/qux");
}

#[tokio::test]
async fn test_request_get_data_points() {
    let mut mock = MockHttpBase::default();
    let config = load_config().unwrap();
    let expected_data_points = vec![PointDatum {
        name: "test".to_string(),
        topic: "foo/bar".to_string(),
    }];
    let expected_data_points_clone = expected_data_points.clone();
    mock.expect_get()
        .times(1)
        .with(eq(get_url(&config)))
        .returning(move |_| Ok(expected_data_points_clone.clone()));

    let actual = get_point_data(&mock, &config).await;
    assert_eq!(actual.unwrap(), expected_data_points);
}
