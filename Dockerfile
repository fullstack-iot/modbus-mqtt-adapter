FROM rust:1.83.0-slim-bookworm
WORKDIR /app
COPY src src
COPY cfg.yml .
COPY Cargo.* ./
RUN cargo install --path .
CMD scada-mqtt-gateway
