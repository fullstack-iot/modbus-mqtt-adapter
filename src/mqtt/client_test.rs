use crate::models::{DataPoint, PointDatum};
use crate::mqtt::{create_message, create_payload, find_topic_by_name};
use chrono::DateTime;

#[test]
fn test_create_payload() {
    let actual = create_payload(true);
    let data_point = serde_json::from_str::<DataPoint<bool>>(actual.as_str()).unwrap();
    assert!(data_point.value);
    assert!(DateTime::parse_from_rfc3339(&data_point.timestamp).is_ok());
}

#[test]
fn test_create_message() {
    let actual = create_message("foo/bar".to_string(), false);
    assert_eq!(actual.topic, "foo/bar");
    assert_eq!(actual.qos, 1);
}

#[test]
fn test_lookup_topic_by_name() {
    let point_data = vec![PointDatum {
        name: "test".to_string(),
        topic: "foo/bar".to_string(),
    }];
    let actual = find_topic_by_name(&point_data, "test").expect("topic not found");
    assert_eq!(actual, "foo/bar");
}
