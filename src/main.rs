mod utils;

use app::gateway;
use app::utils::{load_config, Level};
use std::env;
use std::error::Error;

#[tokio::main(flavor = "multi_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    let config = load_config()?;
    env::set_var("RUST_LOG", "paho_mqtt=warn,reqwest=warn");
    env_logger::init();

    let subscriber = tracing_subscriber::fmt()
        .with_max_level(match config.log_level {
            Level::ERROR => tracing::Level::ERROR,
            Level::WARN => tracing::Level::WARN,
            Level::INFO => tracing::Level::INFO,
            Level::DEBUG => tracing::Level::DEBUG,
        })
        .with_target(false)
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .expect("Failed to set global default subscriber");

    tracing::info!("Running with: {:?}", config);
    gateway(config).await
}
