mod client;
pub use client::{modbus_read_discrete_input, Modbus};
#[cfg(test)]
mod client_test;
#[cfg(test)]
pub use client::{create_param_and_range, MockModbusBase};
