use crate::models::{DataPoint, PointDatum};
use crate::utils::Config;
use chrono::Utc;

const QOS: i32 = 1;

#[derive(Debug, Clone, PartialEq)]
pub struct Message {
    pub topic: String,
    pub payload: String,
    pub qos: i32,
}

// helpers
pub fn create_payload(value: bool) -> String {
    let timestamp = Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Millis, true);
    let data_point = DataPoint { value, timestamp };
    serde_json::to_string(&data_point).expect("Unable to serialize data point")
}

pub fn create_message(topic: String, value: bool) -> Message {
    Message {
        topic,
        payload: create_payload(value),
        qos: QOS,
    }
}

pub fn find_topic_by_name(point_data: &[PointDatum], name: &str) -> Option<String> {
    point_data
        .iter()
        .find(|pd| pd.name == name)
        .map(|pd| pd.topic.clone())
}

// clients

pub struct Mqtt {
    pub client: paho_mqtt::Client,
}

impl Mqtt {
    pub fn init(config: &Config) -> Self {
        let client =
            paho_mqtt::Client::new(&*config.mqtt_url).expect("Unable to create MQTT client");
        let conn_opts = paho_mqtt::ConnectOptions::new();
        if let Err(e) = client.connect(conn_opts) {
            tracing::warn!("Unable to connect to MQTT broker: {:?}", e);
        }
        Self { client }
    }
    pub fn publish(&self, message: &Message) {
        let msg = paho_mqtt::Message::new(
            &message.topic,
            message.payload.as_bytes().to_vec(),
            message.qos,
        );
        let _ = self.client.publish(msg);
    }
}
