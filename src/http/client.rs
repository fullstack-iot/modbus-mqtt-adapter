#![allow(async_fn_in_trait)]
use crate::utils::Config;
use crate::{Device, PointDatum};
use mockall::automock;
use std::error::Error;
use std::time::Duration;

pub struct Http {
    pub client: reqwest::Client,
}

#[automock]
pub trait HttpBase {
    async fn get(&self, url: String) -> Result<Vec<PointDatum>, Box<dyn Error>>;
}

impl HttpBase for Http {
    async fn get(&self, url: String) -> Result<Vec<PointDatum>, Box<dyn Error>> {
        let device = self.client.get(url).send().await?.json::<Device>().await?;
        Ok(device.point_data)
    }
}

impl Http {
    pub fn init() -> Self {
        Self {
            client: reqwest::Client::new(),
        }
    }
}

pub fn get_url(config: &Config) -> String {
    format!("{}/devices-0/{}", config.api_url, config.device_0_id)
}

pub async fn get_point_data<H: HttpBase>(
    http: &H,
    config: &Config,
) -> Result<Vec<PointDatum>, Box<dyn Error>> {
    //retry mechanism for race condition during startup
    for _ in 0..3 {
        match http.get(get_url(config)).await {
            Ok(point_data) => return Ok(point_data),
            Err(e) => {
                tracing::warn!("Unable to get point data: {:?}", e);
                tokio::time::sleep(Duration::from_secs(5)).await;
            }
        }
    }
    Err("Unable to get point data".into())
}
