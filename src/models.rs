use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct DataPoint<T> {
    pub value: T,
    pub timestamp: String,
}

#[derive(Deserialize, Debug, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PointDatum {
    pub name: String,
    pub topic: String,
}
