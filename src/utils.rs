use serde::{Deserialize, Serialize};
use serde_yaml::Value;
use std::error::Error;
use std::{env, fs};
use validator::Validate;

#[derive(Debug, PartialEq, Deserialize, Serialize, Clone)]
#[allow(clippy::upper_case_acronyms)]
pub enum Level {
    ERROR,
    WARN,
    INFO,
    DEBUG,
}

#[derive(Debug, PartialEq, Deserialize, Serialize, Clone)]
#[allow(non_camel_case_types)]
pub enum Mode {
    development,
    staging,
}

#[derive(Validate, Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct PointMap {
    pub name: String,
    pub server_unit_id: u8,
    pub start_address: u16,
    pub count: u16,
}

#[derive(Validate, Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct Config {
    pub log_level: Level,
    #[validate(range(min = 80, max = 65535))]
    pub server_port: u16,
    pub server_host: String,
    pub system_rate: u64,
    pub device_0_id: String,
    pub point_maps: Vec<PointMap>,
    #[validate(url)]
    pub api_url: String,
    #[validate(url)]
    pub mqtt_url: String,
    #[validate(url)]
    pub ocpp_url: String,
    pub mode: Mode,
}

#[derive(Debug, Deserialize, Serialize)]
struct ConfigMap {
    development: Config,
    staging: Config,
}

#[allow(dead_code)]
pub fn load_config() -> Result<Config, Box<dyn Error>> {
    let contents = fs::read_to_string("cfg.yml").unwrap();
    let mut config_map: Value = serde_yaml::from_str(&contents).unwrap();
    config_map.apply_merge().unwrap();
    let config_map: ConfigMap = serde_yaml::from_value(config_map).unwrap();

    let environment = env::var("ENV").unwrap_or_else(|_| "development".to_string());
    let config = match environment.as_str() {
        "staging" => config_map.staging,
        _ => config_map.development,
    };
    match config.validate() {
        Ok(_) => (),
        Err(e) => panic!("{}", e),
    };
    Ok(config)
}
