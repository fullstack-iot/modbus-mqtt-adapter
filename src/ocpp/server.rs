use crate::utils::Config;
use futures_util::{SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::mpsc;
use tokio::sync::Mutex;
use tokio::time::timeout;
use tokio_tungstenite::{connect_async, tungstenite::protocol::Message};
use url::Url;

const RESPONSE_TIMEOUT: u64 = 10; // 10 seconds timeout

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct OCPPMessage {
    message_type_id: i32,
    unique_id: String,
    action: String,
    payload: serde_json::Value,
}

struct Ocpp {
    tx: mpsc::UnboundedSender<Message>,
    rx: Arc<Mutex<mpsc::UnboundedReceiver<Message>>>,
}

impl Ocpp {
    #[allow(dead_code)]
    pub async fn init(config: &Config) -> Result<Self, Box<dyn Error>> {
        let url = Url::parse(config.ocpp_url.as_str())?;

        let (ws_stream, _) = connect_async(url).await?;
        let (mut write, mut read) = ws_stream.split();

        let (tx, rx) = mpsc::unbounded_channel();
        let rx = Arc::new(Mutex::new(rx));

        let tx_clone = tx.clone();
        let rx_clone = rx.clone();
        // Forward incoming WebSocket messages to the channel
        tokio::spawn(async move {
            while let Some(message) = read.next().await {
                if let Ok(msg) = message {
                    let _ = tx.send(msg);
                }
            }
        });

        // Send outgoing messages from the channel to the WebSocket
        tokio::spawn(async move {
            let mut rx = rx.lock().await;
            while let Some(message) = rx.recv().await {
                write.send(message).await.unwrap();
            }
        });

        Ok(Self {
            tx: tx_clone,
            rx: rx_clone,
        })
    }

    #[allow(dead_code)]
    async fn send_message(&self, message: OCPPMessage) -> Result<OCPPMessage, Box<dyn Error>> {
        let message_json = serde_json::to_string(&message)?;
        self.tx.send(Message::Text(message_json))?;

        let response = timeout(Duration::from_secs(RESPONSE_TIMEOUT), async {
            let mut rx = self.rx.lock().await;
            rx.recv().await
        })
        .await
        .unwrap()
        .unwrap();

        let response_text = match response {
            Message::Text(text) => text,
            _ => return Err("Unexpected message type".into()),
        };

        let response_message: OCPPMessage = serde_json::from_str(&response_text)?;

        Ok(response_message)
    }
}
