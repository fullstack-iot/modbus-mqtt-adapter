# SCADA MQTT Gateway 🦀

![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/modbus-mqtt-adapter?branch=01-modbus-record-discrete-input&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/fullstack-iot/modbus-mqtt-adapter/01-modbus-record-discrete-input?style=for-the-badge)
![](https://img.shields.io/badge/rust-1.83.0-gray?style=for-the-badge&logo=rust)

> This tool is a SCADA MQTT Gateway that reads data from Modbus, DNP3, and OCPP servers and publishes it to an MQTT
> broker.

## Pre-Requisites

 ```shell
cargo install cargo-cmd \
               cargo-audit \
               cargo-udeps --locked \
               cargo-tarpaulin \
               cargo2junit \
               cargo-watch
```

## Diagrams

### Sequence

#### Read-only Operations

```plantuml
participant domain_api
participant modbus_server
participant dnp3_outstation
participant ocpp_station
participant gateway
participant mqtt_broker
    
gateway -> domain_api: GET /topics
gateway -> modbus_server: reads registers
gateway <- dnp3_outstation: analog input
gateway <- ocpp_station: request
gateway -> ocpp_station: response
gateway -> mqtt_broker: publish
```