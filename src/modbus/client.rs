#![allow(async_fn_in_trait)]
use crate::utils::Config;
use mockall::automock;
use rand::Rng;
use std::error::Error;
use std::fmt::Debug;
use tokio::time::{sleep, Duration};

const RESPONSE_TIMEOUT: u64 = 1;

#[derive(Debug, Clone, PartialEq)]
pub struct RequestParam {
    pub id: u8,
    response_timeout: Duration,
}

#[derive(Debug, Clone, PartialEq)]
pub struct AddressRange {
    pub start: u16,
    pub count: u16,
}

#[automock]
pub trait ModbusBase {
    async fn read_discrete_input(
        &mut self,
        param: RequestParam,
        range: AddressRange,
    ) -> Result<bool, Box<dyn Error>>;
}
struct LoggingListener;

impl<T> rodbus::client::Listener<T> for LoggingListener
where
    T: Debug,
{
    fn update(&mut self, value: T) -> rodbus::MaybeAsync<()> {
        tracing::info!("Channel Listener: {:?}", value);
        rodbus::MaybeAsync::ready(())
    }
}

pub struct Modbus {
    pub client: rodbus::client::Channel,
}

impl Modbus {
    pub fn init(config: &Config) -> Self {
        let channel = rodbus::client::spawn_tcp_client_task(
            rodbus::client::HostAddr::dns(config.server_host.clone(), config.server_port),
            1,
            rodbus::default_retry_strategy(),
            rodbus::DecodeLevel::default(),
            Some(Box::new(LoggingListener)),
        );

        Self { client: channel }
    }
    pub async fn enable(&self) -> Result<(), Box<dyn Error>> {
        match self.client.enable().await {
            Ok(_) => Ok(()),
            Err(e) => Err(Box::new(e)),
        }
    }
}

impl ModbusBase for Modbus {
    async fn read_discrete_input(
        &mut self,
        param: RequestParam,
        range: AddressRange,
    ) -> Result<bool, Box<dyn Error>> {
        let rodbus_param = rodbus::client::RequestParam::new(
            rodbus::UnitId::new(param.id),
            param.response_timeout,
        );
        let rodbus_range =
            rodbus::AddressRange::try_from(range.start, range.count).expect("Invalid range");

        let max_retries = 5;
        let base_delay = Duration::from_millis(100);
        let max_delay = Duration::from_secs(30);

        for attempt in 0..max_retries {
            match self
                .client
                .read_discrete_inputs(rodbus_param, rodbus_range)
                .await
            {
                Ok(values) => {
                    if attempt > 0 {
                        tracing::info!(
                            "Successfully read discrete input after {} retry(ies)",
                            attempt
                        );
                    }
                    return Ok(values.first().unwrap().value);
                }
                Err(e) => {
                    if attempt == max_retries - 1 {
                        tracing::error!(
                            "Unable to read discrete input after {} attempts: {:?}",
                            max_retries,
                            e
                        );
                        return Err(Box::new(e));
                    }

                    let backoff = base_delay * 2u32.pow(attempt as u32);
                    let jitter = Duration::from_millis(rand::rng().random_range(0..=100));
                    let delay = std::cmp::min(backoff + jitter, max_delay);

                    tracing::warn!(
                        "Unable to read discrete input (attempt {}): {:?}. Retrying in {:?}...",
                        attempt + 1,
                        e,
                        delay
                    );
                    sleep(delay).await;
                }
            }
        }

        unreachable!()
    }
}

pub fn create_param_and_range(config: &Config, index: usize) -> (RequestParam, AddressRange) {
    let param = RequestParam {
        id: config.point_maps[index].server_unit_id,
        response_timeout: Duration::from_secs(RESPONSE_TIMEOUT),
    };
    let range = AddressRange {
        start: config.point_maps[index].start_address,
        count: config.point_maps[index].count,
    };
    (param, range)
}

pub async fn modbus_read_discrete_input<Mo: ModbusBase>(
    modbus: &mut Mo,
    config: &Config,
    index: usize,
) -> Result<bool, Box<dyn Error>> {
    let (param, range) = create_param_and_range(config, index);
    modbus.read_discrete_input(param, range).await
}
