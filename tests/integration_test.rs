use app::models::DataPoint;
use app::utils::{load_config, Config};
use app::{gateway, Device};
use chrono::DateTime;
use paho_mqtt::{Client, ConnectOptions};
use std::error::Error;
use testcontainers::core::{IntoContainerPort, WaitFor};
use testcontainers::{runners::AsyncRunner, ContainerAsync, GenericImage, ImageExt};

async fn get_api() -> Result<ContainerAsync<GenericImage>, Box<dyn Error>> {
    let api = GenericImage::new("fsiot/domain-api", "latest")
        .with_exposed_port(3000.tcp())
        .with_wait_for(WaitFor::message_on_stdout(
            "Nest application successfully started",
        ))
        .start()
        .await?;
    Ok(api)
}

async fn get_any_topic_data(port: u16, device_0_id: &String) -> Result<String, Box<dyn Error>> {
    let request = reqwest::Client::new();
    let point_data = request
        .get(format!("http://0.0.0.0:{}/devices-0/{}", port, device_0_id))
        .send()
        .await?
        .json::<Device>()
        .await
        .expect("Unable to get point data")
        .point_data;
    Ok(point_data.first().unwrap().topic.clone())
}

async fn get_broker() -> Result<ContainerAsync<GenericImage>, Box<dyn Error>> {
    let broker = GenericImage::new("emqx/emqx", "latest")
        .with_exposed_port(1883.tcp())
        .with_wait_for(WaitFor::message_on_stdout(
            "Listener tcp:default on 0.0.0.0:1883 started.",
        ))
        .start()
        .await?;
    Ok(broker)
}

async fn get_modbus_server() -> Result<ContainerAsync<GenericImage>, Box<dyn Error>> {
    let server = GenericImage::new("fsiot/mock-modbus-server", "latest")
        .with_exposed_port(5002.tcp())
        .with_wait_for(WaitFor::message_on_stdout("Running with"))
        .with_env_var("ENV", "staging")
        .start()
        .await?;
    Ok(server)
}

#[tokio::test]
async fn test_integration() -> Result<(), Box<dyn Error>> {
    // arrange
    let broker = get_broker().await?;
    let broker_port = broker.get_host_port_ipv4(1883).await?;
    let api = get_api().await?;
    let api_port = api.get_host_port_ipv4(3000).await?;
    let server = get_modbus_server().await?;
    let server_port = server.get_host_port_ipv4(5002).await?;

    let cfg = load_config()?;
    let config = Config {
        api_url: format!("http://0.0.0.0:{}", api_port),
        mqtt_url: format!("mqtt://0.0.0.0:{}", broker_port),
        server_port,
        ..cfg
    };
    let mqtt = Client::new(format!("mqtt://0.0.0.0:{}", broker_port))?;
    if let Err(e) = mqtt.connect(ConnectOptions::new()) {
        eprintln!("Unable to connect to MQTT broker: {:?}", e);
    }
    let topic = get_any_topic_data(api_port, &config.device_0_id).await?;
    mqtt.subscribe(topic.as_str(), 1)
        .expect("Unable to subscribe to topic");

    let receiver = mqtt.start_consuming();

    // act - publish
    gateway(config).await.expect("Unable to start gateway");

    // assert - expect a message in correct format
    let message = receiver
        .iter()
        .next()
        .expect("Nothing in here")
        .expect("Nothing in here either");
    let payload = message.payload_str();
    let data_point = serde_json::from_str::<DataPoint<bool>>(payload.as_ref()).unwrap();
    assert!(DateTime::parse_from_rfc3339(&data_point.timestamp).is_ok());
    Ok(())
}
