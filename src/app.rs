pub mod http;
pub mod modbus;
pub mod models;
pub mod mqtt;
pub mod ocpp;
pub mod utils;

use crate::http::{get_point_data, Http};
use crate::modbus::{modbus_read_discrete_input, Modbus};
use crate::models::PointDatum;
use crate::mqtt::{create_message, find_topic_by_name, Mqtt};
use crate::utils::Config;
use serde::Deserialize;
use std::error::Error;
use std::fmt::Debug;
use tokio::time::{sleep, Duration};
// models

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Device {
    pub point_data: Vec<PointDatum>,
}

// dependencies

pub async fn gateway(config: Config) -> Result<(), Box<dyn Error>> {
    // initialize clients
    let http = Http::init();
    let mut modbus = Modbus::init(&config);
    let mqtt = Mqtt::init(&config);

    let point_data = get_point_data(&http, &config).await?;
    modbus.enable().await?;

    loop {
        for (index, pm) in config.point_maps.iter().enumerate() {
            let value = modbus_read_discrete_input(&mut modbus, &config, index).await?;
            let topic = find_topic_by_name(&point_data, &pm.name).expect("topic not found");
            let message = create_message(topic, value);
            mqtt.publish(&message);
        }
        if config.mode == utils::Mode::development {
            return Ok(());
        }
        sleep(Duration::from_secs(config.system_rate)).await;
    }
}
