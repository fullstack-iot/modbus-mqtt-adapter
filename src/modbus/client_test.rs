use crate::modbus::{create_param_and_range, modbus_read_discrete_input, MockModbusBase};
use crate::utils::load_config;

#[test]
fn test_create_param_and_range() {
    let config = load_config().unwrap();
    for (index, pm) in config.point_maps.iter().enumerate() {
        let (param, range) = create_param_and_range(&config, index);
        assert_eq!(param.id, index as u8 + 1);
        assert_eq!(range.start, pm.start_address);
        assert_eq!(range.count, pm.count);
    }
}

#[tokio::test]
async fn test_modbus_read_discrete_inputs() {
    let mut mock = MockModbusBase::default();
    let config = load_config().unwrap();
    let (param, range) = create_param_and_range(&config, 0);
    mock.expect_read_discrete_input()
        .times(1)
        .withf(move |p, r| *p == param && *r == range)
        .returning(move |_, _| Ok(true));
    let actual = modbus_read_discrete_input(&mut mock, &config, 0).await;
    assert!(actual.unwrap());
}
